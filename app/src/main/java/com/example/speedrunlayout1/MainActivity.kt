package com.example.speedrunlayout1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.speedrunlayout1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    val KUNCI = "Kunci 1234"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.bLinear.setOnClickListener { toLinear() }
        binding.bRelative.setOnClickListener { toRelative() }
    }

    private fun toLinear() {
        var linearInten = Intent(this, HalamanLinear::class.java)
        linearInten.apply {
            putExtra(KUNCI, binding.tpNama.text.toString())
        }
        startActivity(linearInten)
    }

    private fun toRelative() {
        var relIntent = Intent(this, HalamanRelative::class.java)
        startActivity(relIntent)
    }
}