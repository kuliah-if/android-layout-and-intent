package com.example.speedrunlayout1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.speedrunlayout1.databinding.ActivityHalamanLinearBinding
import com.example.speedrunlayout1.databinding.ActivityMainBinding

class HalamanLinear : AppCompatActivity() {
    private lateinit var binding: ActivityHalamanLinearBinding
    val KUNCI = "Kunci 1234"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHalamanLinearBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        var pesan = intent.getStringExtra(KUNCI)
        binding.tvPesanMasuk.text = pesan

        binding.bYa.setOnClickListener { _ -> run {
            Toast.makeText(applicationContext,"Ya", Toast.LENGTH_LONG)
        } }
        binding.bTidak.setOnClickListener { _ -> run {
            Toast.makeText(applicationContext,"Tidak", Toast.LENGTH_LONG)
        } }

        binding.bKembali.setOnClickListener { _ -> run {
            var mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
        } }
    }
}