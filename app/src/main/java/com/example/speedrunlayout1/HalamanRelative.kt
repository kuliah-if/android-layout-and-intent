package com.example.speedrunlayout1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.speedrunlayout1.databinding.ActivityHalamanRelativeBinding

class HalamanRelative : AppCompatActivity() {
    private lateinit var binding: ActivityHalamanRelativeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHalamanRelativeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.bKembali.setOnClickListener { _ -> run {
            var mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
        } }
    }
}